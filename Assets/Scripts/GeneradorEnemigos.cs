﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneradorEnemigos : MonoBehaviour
{
    public GameObject enemigoPrefab;
    public Transform[] puntoGenerador;
    public float tiempoGeneracion = 5f;
    // Start is called before the first frame update
    void Start()
    {
        puntoGenerador = new Transform[transform.childCount];
        for(int i = 0; i<transform.childCount; i++)
        {
            puntoGenerador[i] = transform.GetChild(i);
        }
        StartCoroutine(AparecerEnemigos());
    }
    
    IEnumerator AparecerEnemigos()
    {
        while(true)
        {
            for(int i = 0; i < puntoGenerador.Length; i++)
            {
                Transform puntoGeneracion = puntoGenerador[i];
                Instantiate(enemigoPrefab, puntoGeneracion.position, puntoGeneracion.rotation);
            }
            yield return new WaitForSeconds(tiempoGeneracion);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
