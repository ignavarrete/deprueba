﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PantallaTiempo : MonoBehaviour
{
    //public Text contador;
    //private float tiempo = 10f;
    //private Text fin;


    [SerializeField]
    private int minutos;

    [SerializeField]
    private int segundos;

    private int m, s;

    [SerializeField]
    private Text Timer;

    private FuncionJugador funcionJugador;

    // Start is called before the first frame update
    void Start()
    {
        funcionJugador = gameObject.GetComponent<FuncionJugador>();
        //    contador.text = " " + tiempo;
        //    fin.enabled = false;
    }

    public void startTimer()
    {
        m = minutos;
        s = segundos;
        writeTimer(m, s);
        Invoke("updateTimer", 1f);
    }
    public void stopTimer()
    {
        CancelInvoke();
    }
    private void updateTimer()
    {
        s--;
        if (s < 0)
        {
            if (m == 0)
            {
                funcionJugador.endGame();
                return;
            }
            else
            {
                m--;
                s = 59;
            }
        }

        writeTimer(m, s);
        Invoke("updateTimer", 1f);
    }

    private void writeTimer(int m, int s)
    {
        if (s < 10)
        {
            Timer.text = m.ToString() + ":0" + s.ToString();
        }
        else
        {
            Timer.text = m.ToString() + ":" + s.ToString();
        }
    }
    // Update is called once per frame
    void Update()
    {
        //tiempo -= Time.deltaTime;
        //contador.text = " " + tiempo.ToString("f0");
    }
}
