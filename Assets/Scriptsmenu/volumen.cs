﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class volumen : MonoBehaviour
{

    public Slider slider;
    public float slidervalue;
    
    
    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("volumenMusica", 0.5f);
        AudioListener.volume = slider.value;
    }

    public void ChangeSlider( float valor )
    {
        slidervalue = valor;
        PlayerPrefs.SetFloat("volumenMusica", slidervalue);
        AudioListener.volume = slider.value;
    }
   
}
