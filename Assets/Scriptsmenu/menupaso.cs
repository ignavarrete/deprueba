﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class menupaso : MonoBehaviour

{
    private void Awake()
    {
        var noDestruirEntreEsecenas = FindObjectsOfType<menupaso>();

        if (noDestruirEntreEsecenas.Length > 1)
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
